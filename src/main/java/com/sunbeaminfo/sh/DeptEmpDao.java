package com.sunbeaminfo.sh;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

public class DeptEmpDao {
	public List<Dept> getDepts() {
		Session session = HbUtil.getSession();
		Query<Dept> q = session.createQuery("from Dept d");
		return q.getResultList();
	}
}
