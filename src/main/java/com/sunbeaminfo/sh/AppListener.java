package com.sunbeaminfo.sh;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppListener implements ServletContextListener {
    public void contextInitialized(ServletContextEvent sce)  { 
         HbUtil.getSession();
    }
    public void contextDestroyed(ServletContextEvent sce)  { 
        HbUtil.shutdown();
   }
}
