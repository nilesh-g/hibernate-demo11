package com.sunbeaminfo.sh;

import java.util.ArrayList;
import java.util.List;

public class DeptEmpBean {
	public List<Dept> getDepts() {
		DeptEmpDao dao = new DeptEmpDao();
		HbUtil.beginTransaction();
		List<Dept> list = new ArrayList<Dept>();
		try {
			list = dao.getDepts();
			HbUtil.commitTransaction();
		} catch (RuntimeException e) {
			HbUtil.rollbackTransaction();
		}
		return list;
	}
}
