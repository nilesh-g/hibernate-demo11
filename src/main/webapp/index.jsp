<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html> 
<html>
<head>
<meta charset="UTF-8">
<title>Depts</title>
</head>
<body>
	<jsp:useBean id="b" class="com.sunbeaminfo.sh.DeptEmpBean"/>
	<c:forEach var="d" items="${b.depts}">
		<div>${d}</div>
	</c:forEach>
</body>
</html>

